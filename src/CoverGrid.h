// A set of different-sized coverage grids for lines on raster image pixels matrix
#ifdef __cplusplus
extern "C" 
{
#endif
	#include "stdio.h"

	#ifndef _SF_COVERGRID_
	#define _SF_COVERGRID_

	typedef struct
	{
		// Grids for which memory is to be allocated
		char *coverGrid1;
		char *coverGrid2;
		char *coverGrid3;
		char *coverGrid4;
		char *coverGrid6;
		char *coverGrid8;
		char *coverGrid11;
		char *coverGrid16;
		char *coverGrid22;
		char *coverGrid32;
		char *coverGrid45;
		char *coverGrid64;
		char *coverGrid90;
		char *coverGrid128;

		// Current working grid pointer and step (size)
		char *coverGridActive;
		int sizeActive;

		// External frame size and image dimensions (without external frame)
		int frameWidthGrid, wGrid, hGrid;

		// Grid cell coordinates
		int sX_i, sY_i;

		// Full grids sizes including external frame
		int hGrid1, hGrid2, hGrid3, hGrid4, hGrid6, hGrid8, hGrid11, hGrid16, hGrid22,
		 hGrid32, hGrid45, hGrid64, hGrid90, hGrid128;
	} CoverGrid;

	// Memory allocation
	void PrepareCoverGrid(CoverGrid *covGrid, int w_, int h_, int frameWidth_);

	// Set coverage of a point
	void Cover(CoverGrid *covGrid, double sX, double sY, double maxSize);

	// Get coverage of a point
	int Coverage(CoverGrid *covGrid, double sX, double sY);

	// Select grid with a specified step (size)
	void SelectActiveGrid(CoverGrid *covGrid, double step);

	#endif
#ifdef __cplusplus
}
#endif