// Extracts curves (geometrical primitives) from raw curves of points
#ifdef __cplusplus
extern "C" 
{
#endif
	#include <stdio.h>
	#include "stdlib.h"
	#include <math.h>
	#include "RawCurve.h"
	#include "Curve.h"
	#include "FastVectorizator.h"

	#ifndef _SF_CURVESEXTRACOR_
	#define _SF_CURVESEXTRACOR_

	typedef struct
	{
		Curve *curves[19999];
		int curvesCounter;

		// Directions between all couples of the previous and current point (equal points angles of raw curves)
		float pointsAngles[19999];

		RawCurve *currentRawCurve;
	} CurvesExtractor;

	// Initializes extraction variables and allocates memory for curves
	void PrepareCurvesExtractor(CurvesExtractor *cE);

	// Main function of curves extraction of raw curves
	int EctractCurves(CurvesExtractor *cE, FastVectorizator *fV, int rawCurvesQuantity);

	// Recursive function which divides raw curve into fragments which then are approximated by curves
	int SearchExtractCurve(int startID, int finishID);

	int GetCurvesQuantity();

	#endif
#ifdef __cplusplus
}
#endif