#include "stdio.h"
#include "stdlib.h"
#include "CoverGrid.h"

void PrepareCoverGrid(CoverGrid *covGrid, int w_, int h_, int frameWidth_)
{
	covGrid->sizeActive = 128;
    covGrid->frameWidthGrid = frameWidth_;
    covGrid->wGrid = w_; covGrid->hGrid = h_;

    // making cover grids
	covGrid->coverGrid1 = (char*)calloc((w_ + 2 * frameWidth_) / 1 * (h_ + 2 * frameWidth_) / 1, sizeof(char));
	covGrid->coverGrid2 = (char*)calloc((w_ + 2 * frameWidth_) / 2 * (h_ + 2 * frameWidth_) / 2, sizeof(char));
	covGrid->coverGrid3 = (char*)calloc((w_ + 2 * frameWidth_) / 3 * (h_ + 2 * frameWidth_) / 3, sizeof(char));
	covGrid->coverGrid4 = (char*)calloc((w_ + 2 * frameWidth_) / 4 * (h_ + 2 * frameWidth_) / 4, sizeof(char));
	covGrid->coverGrid6 = (char*)calloc((w_ + 2 * frameWidth_) / 6 * (h_ + 2 * frameWidth_) / 6, sizeof(char));
	covGrid->coverGrid8 = (char*)calloc((w_ + 2 * frameWidth_) / 8 * (h_ + 2 * frameWidth_) / 8, sizeof(char));
	covGrid->coverGrid11 = (char*)calloc((w_ + 2 * frameWidth_) / 11 * (h_ + 2 * frameWidth_) / 11, sizeof(char));
	covGrid->coverGrid16 = (char*)calloc((w_ + 2 * frameWidth_) / 16 * (h_ + 2 * frameWidth_) / 16, sizeof(char));
	covGrid->coverGrid22 = (char*)calloc((w_ + 2 * frameWidth_) / 22 * (h_ + 2 * frameWidth_) / 22, sizeof(char));
	covGrid->coverGrid32 = (char*)calloc((w_ + 2 * frameWidth_) / 32 * (h_ + 2 * frameWidth_) / 32, sizeof(char));
	covGrid->coverGrid45 = (char*)calloc((w_ + 2 * frameWidth_) / 45 * (h_ + 2 * frameWidth_) / 45, sizeof(char));
	covGrid->coverGrid64 = (char*)calloc((w_ + 2 * frameWidth_) / 64 * (h_ + 2 * frameWidth_) / 64, sizeof(char));
	covGrid->coverGrid90 = (char*)calloc((w_ + 2 * frameWidth_) / 90 * (h_ + 2 * frameWidth_) / 90, sizeof(char));
	covGrid->coverGrid128 = (char*)calloc((w_ + 2 * frameWidth_) / 128 * (h_ + 2 * frameWidth_) / 128, sizeof(char));

	covGrid->hGrid1 = (h_ + 2 * frameWidth_) / 1;
	covGrid->hGrid2 = (h_ + 2 * frameWidth_) / 2;
	covGrid->hGrid3 = (h_ + 2 * frameWidth_) / 3;
	covGrid->hGrid4 = (h_ + 2 * frameWidth_) / 4;
	covGrid->hGrid6 = (h_ + 2 * frameWidth_) / 6;
	covGrid->hGrid8 = (h_ + 2 * frameWidth_) / 8;
	covGrid->hGrid11 = (h_ + 2 * frameWidth_) / 11;
	covGrid->hGrid16 = (h_ + 2 * frameWidth_) / 16;
	covGrid->hGrid22 = (h_ + 2 * frameWidth_) / 22;
	covGrid->hGrid32 = (h_ + 2 * frameWidth_) / 32;
	covGrid->hGrid45 = (h_ + 2 * frameWidth_) / 45;
	covGrid->hGrid64 = (h_ + 2 * frameWidth_) / 64;
	covGrid->hGrid90 = (h_ + 2 * frameWidth_) / 90;
	covGrid->hGrid128 = (h_ + 2 * frameWidth_) / 128;
}

void Cover(CoverGrid *covGrid, double sX, double sY, double maxSize)
{
    int lowX = ((int)sX - 1 >= 0) ? ((int)sX - 1) : covGrid->frameWidthGrid;
    int lowY = ((int)sY - 1 >= 0) ? ((int)sY - 1) : covGrid->frameWidthGrid;
    int highX = ((int)sX + 1 < covGrid->wGrid + covGrid->frameWidthGrid) 
		? ((int)sX + 1) : (covGrid->wGrid + covGrid->frameWidthGrid - 1);
    int highY = ((int)sY + 1 < covGrid->hGrid + covGrid->frameWidthGrid) 
		? ((int)sY + 1) : (covGrid->hGrid + covGrid->frameWidthGrid - 1);
    double e = 0.5, k = 0.5;
	int x, y;
	double correctedSize = (maxSize + e) / k;
    for (x = lowX; x < highX; ++x)
        for (y = lowY; y < highY; ++y)
        {
            if (correctedSize > 1)
                covGrid->coverGrid1[x/1 * covGrid->hGrid1 + y / 1] = 1;
            if (correctedSize > 2)
                covGrid->coverGrid2[x/2 * covGrid->hGrid2 + y / 2] = 1;
            if (correctedSize > 3)
                covGrid->coverGrid3[x/3 * covGrid->hGrid3 + y / 3] = 1;
            if (correctedSize > 4)
                covGrid->coverGrid4[x/4 * covGrid->hGrid4 + y / 4] = 1;
            if (correctedSize > 6)
                covGrid->coverGrid6[x/6 * covGrid->hGrid6 + y / 6] = 1;
            if (correctedSize > 8)
                covGrid->coverGrid8[x/8 * covGrid->hGrid8 + y / 8] = 1;
            if (correctedSize > 11)
                covGrid->coverGrid11[x/11 * covGrid->hGrid11 + y / 11] = 1;
            if (correctedSize > 16)
                covGrid->coverGrid16[x/16 * covGrid->hGrid16 + y / 16] = 1;
            if (correctedSize > 22)
                covGrid->coverGrid22[x/22 * covGrid->hGrid22 + y / 22] = 1;
            if (correctedSize > 32)
                covGrid->coverGrid32[x/32 * covGrid->hGrid32 + y / 32] = 1;
            if (correctedSize > 45)
                covGrid->coverGrid45[x/45 * covGrid->hGrid45 + y / 45] = 1;
            if (correctedSize > 64)
                covGrid->coverGrid64[x/64 * covGrid->hGrid64 + y / 64] = 1;
            if (correctedSize > 90)
                covGrid->coverGrid90[x/90 * covGrid->hGrid90 + y / 90] = 1;
            if (correctedSize > 128)
                covGrid->coverGrid128[x/128 * covGrid->hGrid128 + y / 128] = 1;
        }
}

int Coverage(CoverGrid *covGrid, double sX, double sY)
{
    covGrid->sX_i = (int)sX; covGrid->sY_i = (int)sY;
    if (covGrid->coverGrid1[covGrid->sX_i * covGrid->hGrid1 + covGrid->sY_i]
        || covGrid->coverGrid2[covGrid->sX_i/ 2 * covGrid->hGrid2 + covGrid->sY_i / 2]
        || covGrid->coverGrid3[covGrid->sX_i/ 3 * covGrid->hGrid3 + covGrid->sY_i / 3]
        || covGrid->coverGrid4[covGrid->sX_i/ 4 * covGrid->hGrid4 + covGrid->sY_i / 4]
        || covGrid->coverGrid6[covGrid->sX_i/ 6 * covGrid->hGrid6 + covGrid->sY_i / 6]
        || covGrid->coverGrid8[covGrid->sX_i/ 8 * covGrid->hGrid8 + covGrid->sY_i / 8]
        || covGrid->coverGrid11[covGrid->sX_i/ 11 * covGrid->hGrid11 + covGrid->sY_i / 11]
        || covGrid->coverGrid16[covGrid->sX_i/ 16 * covGrid->hGrid16 + covGrid->sY_i / 16]
        || covGrid->coverGrid22[covGrid->sX_i/ 22 * covGrid->hGrid22 + covGrid->sY_i / 22]
        || covGrid->coverGrid32[covGrid->sX_i/ 32 * covGrid->hGrid32 + covGrid->sY_i / 32]
        || covGrid->coverGrid45[covGrid->sX_i/ 45 * covGrid->hGrid45 + covGrid->sY_i / 45]
        || covGrid->coverGrid64[covGrid->sX_i/ 64 * covGrid->hGrid64 + covGrid->sY_i / 64]
        || covGrid->coverGrid90[covGrid->sX_i/ 90 * covGrid->hGrid90 + covGrid->sY_i / 90]
        || covGrid->coverGrid128[covGrid->sX_i/ 128 * covGrid->hGrid128 + covGrid->sY_i / 128])
        return 1;
    else
        return 0;
}

void SelectActiveGrid(CoverGrid *covGrid, double step)
{
    if (step < 1)
    {
        covGrid->coverGridActive = covGrid->coverGrid1;
        covGrid->sizeActive = 1;
    }
    else if (step < 2)
    {
        covGrid->coverGridActive = covGrid->coverGrid2;
        covGrid->sizeActive = 2;
    }
    else if (step < 4)
    {
        covGrid->coverGridActive = covGrid->coverGrid4;
        covGrid->sizeActive = 4;
    }
    else if (step < 6)
    {
        covGrid->coverGridActive = covGrid->coverGrid6;
        covGrid->sizeActive = 6;
    }
    else if (step < 8)
    {
        covGrid->coverGridActive = covGrid->coverGrid8;
        covGrid->sizeActive = 8;
    }
    else if (step < 11)
    {
        covGrid->coverGridActive = covGrid->coverGrid11;
        covGrid->sizeActive = 11;
    }
    else if (step < 16)
    {
        covGrid->coverGridActive = covGrid->coverGrid16;
       covGrid->sizeActive = 16;
    }
    else if (step < 22)
    {
        covGrid->coverGridActive = covGrid->coverGrid22;
        covGrid->sizeActive = 22;
    }
    else if (step < 32)
    {
        covGrid->coverGridActive = covGrid->coverGrid32;
        covGrid->sizeActive = 32;
    }
    else if (step < 45)
    {
        covGrid->coverGridActive = covGrid->coverGrid45;
        covGrid->sizeActive = 45;
    }
    else if (step < 64)
    {
        covGrid->coverGridActive = covGrid->coverGrid64;
        covGrid->sizeActive = 64;
    }
    else if (step < 90)
    {
        covGrid->coverGridActive = covGrid->coverGrid90;
        covGrid->sizeActive = 90;
    }
    else
    {
        covGrid->coverGridActive = covGrid->coverGrid128;
        covGrid->sizeActive = 128;
    }
}