// A curve which is formed by a chain of points, and its point
#ifdef __cplusplus
extern "C" 
{
#endif
	#include "stdio.h"
	#include "CoverGrid.h"

	#ifndef _SF_RAWCURVE_
	#define _SF_RAWCURVE_

	// A point of a raw curve
	typedef struct
	{
		float x;
		float y;
		float angle;

		// Cumulative length from the beginning of the raw curve to the current point
		float lengthIntegral;
	} Vertex;

	// A raw curve, itself
	typedef struct
	{
		// Maximal and minimal raw curve direction
		float angleMax, angleMin;

		// Point of a chain
		Vertex curvePoints[501];

		// Current vertex count, and vertices quantity
		int vertexID;

		// Backward-motion start point index
		int backVertexID;

		// Current length of line and current direction
		double length;
		float angleCurrent;
	} RawCurve;

	// Initializes raw curve parameters
	void PrepareRawCurve(RawCurve *rawCurve);

	int VerticesQuantity(RawCurve *rawCurve);

	void SetBackVertexID(RawCurve *rawCurve);

	// Covers current line with coverage grid to the last point
	void CoverFull(RawCurve *rawCurve, CoverGrid *covGrid, float stepSize);

	// Adds a new vertex to line
	void WriteVertex(RawCurve *currentCurve, CoverGrid *covGrid, float sX, float sY, float angle, double step);

	// Removes the specified quantity of last points from raw curve
	void TrimVertices(RawCurve *rawCurve, int quantity);

	#endif
#ifdef __cplusplus
}
#endif