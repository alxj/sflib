#include <stdio.h>
#include "stdlib.h"
#include <math.h>
#include "RawCurve.h"
#include "CoverGrid.h"
#include "FastVectorizator.h"

static short *pixels;

// parameters of algorithm
static int maxVerticesInCurve;
static int maxCurvesNumber;
static int searchDepth;
static double searchContrast;

// other local variables
static FastVectorizator *fV;
static int frameWidth;
static int totalPointsCounter;
static float weights[31];
static float levels[31];
static float dirRating[31];
static float fSin[720], fCos[720];
static float pi, twoPi;
static double lastStep;
static double coverageSize;
static int xCurrent, yCurrent, xShift;
static int w, h;
static float sX, sY;
static float newX, newY;
static float step;
static int overlap;
static int vertexID;
static int curvesCounter;
static int pointsCounter;
static float angle;
static float anglePrev;
static float angleStart;
static int angleDeg;
static int realW, realH;
static int level = 9;

int PrepareVectorizator(FastVectorizator *fV_, CoverGrid *covGrid)
{
	fV = fV_;
	fV->covGrid = covGrid;

	for(int i = 0; i < 10001; ++i)
		fV->curves[i] = (RawCurve*)malloc(sizeof(RawCurve));

	// parameters of algorithm
	maxVerticesInCurve = 499;
	fV->maxCurvesNumber = maxCurvesNumber = 9999;
	searchDepth = 6;
	searchContrast = 0.85;

	frameWidth = 128;
	totalPointsCounter = 1;
	pi = (float)3.1415926; twoPi = (float)(2.0 * 3.1415926);
	lastStep = 1;
	sX = 0, sY = 0;
	newX = 0, newY = 0;
	step = 1;
	overlap = 0;
	vertexID = 0;
	curvesCounter = 0;
	pointsCounter = 0;
	angle = 0;
	anglePrev = 0;
	angleStart = 0;
	return 0;
}

void PreCalculateTrigonometry()
{
    // fast sin cos table
	int cnt;
    for (cnt = 0; cnt < 720; ++cnt)
    {
        fSin[cnt] = (float)sin(cnt / twoPi);
        fCos[cnt] = (float)cos(cnt / twoPi);
    }
}

int GetRawCurvesQuantity()
{
    return fV->curvesCounter;
}

int GetPointsQuantity()
{
    return fV->pointsCounter;
}

int GetTotalPointsQuantity()
{
    return fV->totalPointsCounter;
}

void SelectGrid()
{
    SelectActiveGrid(fV->covGrid, step);
}

int RawVectorize(FastVectorizator *fV_, short *pixels_, int w_, int h_, int frameWidth_)
{
	fV = fV_;
    fV->pixels = pixels = pixels_;

    frameWidth = frameWidth_;
    w = w_; h = h_;
	PreCalculateTrigonometry();

    // preparing Gauss window
	double sigma = 3.0, m = 15.0;
    for (int wCnt = 0 + level; wCnt < 31 - level; ++wCnt)
        weights[wCnt] = (float)exp(-(wCnt - m) * (wCnt - m) / 2.0 / sigma / sigma);

    // tracking curves on entire image
    curvesCounter = 0;
    pointsCounter = 0;
    totalPointsCounter = 1;
    lastStep = 0;
	realW = w + frameWidth;
	realH = h + frameWidth;
    for (xCurrent = frameWidth; xCurrent < realW; xCurrent += 1)
	{
		xShift = xCurrent * realH;
        for (yCurrent = frameWidth + xCurrent % 2; yCurrent < realH; yCurrent += 2)
            if (pixels[xShift + yCurrent] < pixels[xShift - 2 * realH + yCurrent] * searchContrast
            || pixels[xShift + yCurrent] < pixels[xShift +  yCurrent - 2] * searchContrast)
                if (curvesCounter < maxCurvesNumber)
                    TrackCurve();
	}
	fV->curvesCounter = curvesCounter;
	fV->pointsCounter = pointsCounter;
	fV->totalPointsCounter = totalPointsCounter;
	return curvesCounter;
}

// METHODS FOR ALGORITHM

void TrackCurve()
{
    coverageSize = lastStep;

    if (Coverage(fV->covGrid, xCurrent, yCurrent) > 0)
        return;

	fV->currentCurve = fV->curves[curvesCounter];
	PrepareRawCurve(fV->currentCurve);
	vertexID = 0;

    for (angleStart = 0; angleStart < twoPi - 0.1F; angleStart += pi / 3.0F)
    {
        // preparing to track curve
        sX = (float)xCurrent;
        sY = (float)yCurrent;
        newX = 0;
        newY = 0;
        step = 1;
        overlap = 0;
        vertexID = 0;

        // checking direction
        angle = angleStart;
        if (GoStep() < 0)
            continue;
        angle = angleStart;

        // search tracking
        anglePrev = angle;
        for (vertexID = 0; vertexID < searchDepth; ++vertexID)
        {
            anglePrev = angle;
            if (GoStep() < 0)
                break;
            sX = newX; sY = newY;
            ++totalPointsCounter; // debug
            lastStep += step;
        }
        float backX = sX, backY = sY, backAngle = anglePrev - pi/*, backStep = step*/;

        // forward tracking, when no search failure
        if (vertexID == searchDepth)
        {
			WriteVertex(fV->currentCurve, fV->covGrid, sX, sY, angle, step);
            for (vertexID = 0; vertexID < maxVerticesInCurve; ++vertexID)
            {
                if (GoStep() < 0)
                    break;
                sX = newX; sY = newY; 
				WriteVertex(fV->currentCurve, fV->covGrid, sX, sY, angle, step);
                ++totalPointsCounter; // debug
                lastStep += step;
            }
        }

        // back tracking
        if (searchDepth > 0)
        {
            sX = (float)backX; sY = (float)backY; angle = backAngle; step = 1;
            SetBackVertexID(fV->currentCurve);
            for (vertexID = 0; vertexID < maxVerticesInCurve; ++vertexID)
            {
                if (GoStep() < 0)
                    break;
                sX = newX; sY = newY;
                WriteVertex(fV->currentCurve, fV->covGrid, sX, sY, angle, step);
                ++totalPointsCounter; // debug
                lastStep += step;
            }
        }

        // writing curve
        if (VerticesQuantity(fV->currentCurve) > 2)
        {
            CoverFull(fV->currentCurve, fV->covGrid, step);
            pointsCounter += VerticesQuantity(fV->currentCurve);
            ++curvesCounter;
            break;
        }
        else
        {
            fV->currentCurve->vertexID = 0;
            fV->currentCurve->backVertexID = 9000;
        }
    }
}

int GoStep()
{
    float newX_, newY_;
    if (step < 0.5 || step > 127)
        return -5;

    // counting min and max levels on front, and if overlapped
    float minLevel = 255, maxLevel = 0;
	int dCnt;
    for (dCnt = 0; dCnt < 31; ++dCnt)
    {
        angleDeg = (int)((angle + 0.1 * (dCnt - 15)) * twoPi) % 360 + 360;
        newX_ = sX + step * fCos[angleDeg];
        newY_ = sY - step * fSin[angleDeg];
        if ((levels[dCnt] = (float)(255 - pixels[(int)(newX_) * realH + (int)newY_])) < minLevel)
            minLevel = levels[dCnt];
		else if (levels[dCnt] > maxLevel)
            maxLevel = levels[dCnt];
    }

    // ckecking contrast-based finish
    int sLevel = 255 - pixels[(int)sX * realH + (int)sY];
    if (sLevel < minLevel)
        return -3;
	if (vertexID < searchDepth)
	{
		if (0.95 * maxLevel < minLevel)
			return -3;
	}
	else
	{
		if (0.8 * maxLevel < minLevel)
			return -3;
	}


    // correcting levels by treshold and ratings
    for (dCnt = level; dCnt < 31; ++dCnt)
    {
        if (levels[dCnt] < minLevel + (maxLevel - minLevel) / 1.5)
            dirRating[dCnt] = levels[dCnt] = 0;
        else
        {
            levels[dCnt] -= minLevel;
            dirRating[dCnt] = levels[dCnt] * weights[dCnt];
        }
    }
            
    // counting integral on window
    float sum = 0;
    for (dCnt = 0; dCnt < 31; ++dCnt)
        if (dirRating[dCnt] > 0)
            sum += dirRating[dCnt];
    if (sum == 0)
        return -1;

    // finding median
    float halfSum = 0;
    float median, rightSumD, leftSumD;
    for (median = 0; median < 31; ++median)
    {
        halfSum += dirRating[(int)median];
        if (halfSum >= sum / 2.0)
        {
            rightSumD = halfSum - sum / 2.0F;
            leftSumD = sum / 2.0F - (halfSum - dirRating[(int)median]);
            median -= rightSumD / (rightSumD + leftSumD);
            break;
        }
    }
    angleDeg = (int)((angle + 0.1 * (median - 15.0)) * twoPi) % 360 + 360;
            
           
    newX_ = sX + step * fCos[angleDeg];
    newY_ = sY - step * fSin[angleDeg];

    // checking overlap-based finish
    int isOverlap = 0, maxStopOverlap = 3;
    if (Coverage(fV->covGrid, newX_, newY_) > 0)
        isOverlap = 1;
    if (isOverlap > 0)
        ++overlap;
    else
        overlap = 0;
    if (overlap > maxStopOverlap)
        return -2;
    if ((int)newX_ - frameWidth < 0 || (int)newX_ - frameWidth >= w 
		|| (int)newY_ - frameWidth < 0 || (int)newY_ - frameWidth >= h)
        return -6;

    // changing step
    int lowBound = 0;
    for (dCnt = 0; dCnt < 31; ++dCnt)
        if (dirRating[dCnt] > 0)
        {
            lowBound = dCnt;
            break;
        }
    int highBound = 31;
    for (dCnt = 30; dCnt >= 0; --dCnt)
        if (dirRating[dCnt] > 0)
        {
            highBound = dCnt;
            break;
        }
    int lineWidth = highBound - lowBound;
    if (lineWidth > 0 && lineWidth < 6) // ������ ��� ��� ������ �����������!
        step *= 0.8F;
    else if (lineWidth > 8)
        step *= 1.5F;

    // applying angle
    angle += 0.1F * (median - 15.0F);
    newX = newX_;
    newY = newY_;
    return 0;
}