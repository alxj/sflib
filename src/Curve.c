#include <stdio.h>
#include <math.h>
#include "Curve.h"

int MakeCurve(Curve *crv, double x1_, double y1_, double xM_, double yM_, double x2_, double y2_)
{
	double pi = 3.1415926;
	crv->x1 = x1_; crv->y1 = y1_;
	crv->x2 = x2_; crv->y2 = y2_;
	crv->xM = xM_; crv->yM = yM_;
	double A1 = crv->y1 - crv->yM, B1 = crv->xM - crv->x1/*, C1 = crv->x1 * crv->yM - crv->xM * crv->y1*/;
	double A2 = crv->y2 - crv->yM, B2 = crv->xM - crv->x2/*, C2 = crv->x2 * crv->yM - crv->xM * crv->y2*/;
	if ((fabs(B1) > 1 && fabs(B2) > 1 && abs(A1 / B1 - A2 / B2) < 0.4) // recommended 0.1 - 0.4. Line/arc definition threshold
		// Greater value leads to greater share of lines, smaller share of arcs
		|| (fabs(A1) > 1 && fabs(A2) > 1 && fabs(B1 / A1 - B2 / A2) < 0.4)) // ..the same
	{
		crv->dirAngle = (float)atan2(-crv->y2 + crv->y1, crv->x2 - crv->x1);
		crv->length = (float)sqrt((crv->x2 - crv->x1) * (crv->x2 - crv->x1) + (crv->y2 - crv->y1) * (crv->y2 - crv->y1));
		crv->isArc = 0;
	}
	else
	{
		double xM1 = (crv->x1 + crv->xM) / 2.0, yM1 = (crv->y1 + crv->yM) / 2.0;
		double xM2 = (crv->x2 + crv->xM) / 2.0, yM2 = (crv->y2 + crv->yM) / 2.0;
		double A1p = B1, B1p = -A1, C1p = -(A1p * xM1 + B1p * yM1);
		double A2p = B2, B2p = -A2, C2p = -(A2p * xM2 + B2p * yM2);

		if (fabs(A1p) > fabs(B1p))
		{
			crv->yC = (C1p * A2p / A1p - C2p) / (B2p - B1p * A2p / A1p);
			crv->xC = (-C1p - crv->yC * B1p) / A1p;
		}
		else
		{
			crv->xC = (C1p * B2p / B1p - C2p) / (A2p - A1p * B2p / B1p);
			crv->yC = (-C1p - crv->xC * A1p) / B1p;
		}
		crv->angleStart = atan2(crv->y1 - crv->yC, crv->x1 - crv->xC);
		double angleM = atan2(crv->yM - crv->yC, crv->xM - crv->xC);
		if (angleM - crv->angleStart > pi)
			crv->angleStart += 2 * pi;
		else if (angleM - crv->angleStart < -pi)
			crv->angleStart -= 2 * pi;
		crv->angleFinish = atan2(crv->y2 - crv->yC, crv->x2 - crv->xC);
		if (crv->angleFinish - angleM > pi)
			crv->angleFinish -= 2 * pi;
		else if (crv->angleFinish - angleM < -pi)
			crv->angleFinish += 2 * pi;
		//double diff1 = angleM - crv->angleStart, diff2 = crv->angleFinish - angleM;
		if (crv->angleFinish > crv->angleStart)
			crv->angleTurn = crv->angleFinish - crv->angleStart;
		else
		{
			crv->angleTurn = crv->angleStart - crv->angleFinish;
			crv->angleStart = crv->angleFinish;
		}
		crv->rC = sqrt((crv->x1 - crv->xC) * (crv->x1 - crv->xC) + (crv->y1 - crv->yC) * (crv->y1 - crv->yC));
		crv->length = crv->angleTurn * crv->rC;
		crv->isArc = 1;
	}
	return 0;
}