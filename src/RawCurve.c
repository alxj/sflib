#include "stdio.h"
#include "stdlib.h"
#include "RawCurve.h"

void PrepareRawCurve(RawCurve *rawCurve)
{
	rawCurve->angleMax = 9000;
	rawCurve->angleMin = -9000;
	rawCurve->length = 0;
    rawCurve->vertexID = 0;
    rawCurve->backVertexID = 999999;
}

int VerticesQuantity(RawCurve *rawCurve)
{
    return rawCurve->vertexID;
}

void SetBackVertexID(RawCurve *rawCurve)
{
    rawCurve->backVertexID = rawCurve->vertexID;
}

void CoverFull(RawCurve *rawCurve, CoverGrid *covGrid, float stepSize)
{
    Cover(covGrid, rawCurve->curvePoints[0].x, rawCurve->curvePoints[0].y, stepSize);
    Cover(covGrid, rawCurve->curvePoints[1].x, rawCurve->curvePoints[1].y, stepSize);
    Cover(covGrid, rawCurve->curvePoints[2].x, rawCurve->curvePoints[2].y, stepSize);
    if (rawCurve->vertexID > 0)
        Cover(covGrid, rawCurve->curvePoints[rawCurve->vertexID - 1].x, 
		rawCurve->curvePoints[rawCurve->vertexID - 1].y, stepSize);
    if (rawCurve->vertexID > 1)
        Cover(covGrid, rawCurve->curvePoints[rawCurve->vertexID - 2].x, 
		rawCurve->curvePoints[rawCurve->vertexID - 2].y, stepSize);
    if (rawCurve->vertexID > 2)
        Cover(covGrid, rawCurve->curvePoints[rawCurve->vertexID - 3].x, 
		rawCurve->curvePoints[rawCurve->vertexID - 3].y, stepSize);
}

void WriteVertex(RawCurve *rawCurve, CoverGrid *covGrid, float sX, float sY, float angle, double step)
{
    if (rawCurve->vertexID > 500)
        return;
    
    if (rawCurve->vertexID > 0)
    {
        float anglesDelta = angle - rawCurve->curvePoints[rawCurve->vertexID - 1].angle;
        rawCurve->length += step;
        if (anglesDelta < -3.1415926)
            anglesDelta += (float)(2.0 * 3.1415926);
        else if (anglesDelta > 3.1415926)
            anglesDelta -= (float)(2.0 * 3.1415926);
        rawCurve->angleCurrent = rawCurve->curvePoints[rawCurve->vertexID].angle 
			= rawCurve->curvePoints[rawCurve->vertexID - 1].angle + anglesDelta;
        if (rawCurve->vertexID > 6)
            Cover(covGrid, rawCurve->curvePoints[rawCurve->vertexID - 4].x, 
			rawCurve->curvePoints[rawCurve->vertexID - 4].y, step);
    }
    else
        rawCurve->angleCurrent = rawCurve->curvePoints[rawCurve->vertexID].angle = angle;
    rawCurve->curvePoints[rawCurve->vertexID].x = sX;
    rawCurve->curvePoints[rawCurve->vertexID].y = sY;
    rawCurve->curvePoints[rawCurve->vertexID].lengthIntegral = (float)rawCurve->length;
    if (rawCurve->vertexID > rawCurve->backVertexID)
    {
        if (rawCurve->angleCurrent > rawCurve->angleMax)
            rawCurve->angleMax = rawCurve->angleCurrent;
        else if (rawCurve->angleCurrent < rawCurve->angleMin)
            rawCurve->angleMin = rawCurve->angleCurrent;
    }
    ++rawCurve->vertexID;
}

void TrimVertices(RawCurve *rawCurve, int quantity)
{
	int newVertexID;
    if (quantity < 0)
        return;
    newVertexID = (rawCurve->vertexID < quantity) ? 0 : (rawCurve->vertexID - quantity);
    rawCurve->vertexID = newVertexID;
}