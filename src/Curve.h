// A curve (a geometrical primitive) which can be a line or an arc
#ifdef __cplusplus
extern "C" 
{
#endif
	#include <stdio.h>
	#include "stdlib.h"
	#include <math.h>

	#ifndef _SF_CURVE_
	#define _SF_CURVE_

	typedef struct
	{
		// Coordinates of curve ends
		double x1, y1;
		double x2, y2;

		// Central point of arc
		double xM, yM;

		// Direction and length of line
		double dirAngle;
		double length;

		// Center and radius of arc
		double xC, yC;
		double rC;

		// Arc angles
		double angleStart, angleFinish, angleTurn;

		// Arc or line
		int isArc;
	} Curve;

	// Constructor
	int MakeCurve(Curve *crv, double x1_, double y1_, double xM_, double yM_, double x2_, double y2_);

	#endif
#ifdef __cplusplus
}
#endif