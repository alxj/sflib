// SFlib - Sparse Front library for line raster images vectorization.
// API for external access. Also, the entry points for the library.
// Version: 2013, build 2
// About: by Alexey Kovtun 2012-2013, under GNU GPL v3 license
#ifdef __cplusplus
extern "C" 
{
#endif
	#include <stdio.h>
	#include "FastVectorizator.h"
	#include "CurvesExtractor.h"

	// Static structures of coverage grids, raw curves extractor, curves extractor
	static CoverGrid *covGrid;
	static FastVectorizator *fV;
	static CurvesExtractor *cE;

	// Allocates memory and prepares extractors for a raster image w * h.
	// Currently, frameWidth = 128.
	// When processing a raster image, use this fucntion first
	int SFAllocMem(int w, int h, int frameWidth)
	{
		covGrid = (CoverGrid*)malloc(sizeof(CoverGrid));
		PrepareCoverGrid(covGrid, w, h, frameWidth);
		if (covGrid == NULL)
			return 1;

		fV = (FastVectorizator*)malloc(sizeof(FastVectorizator));
		PrepareVectorizator(fV, covGrid);
		if (fV == NULL)
			return 2;

		cE = (CurvesExtractor*)malloc(sizeof(CurvesExtractor));
		PrepareCurvesExtractor(cE);
		if (cE == NULL)
			return 3;
		return 0;
	}

	// Extracts raw curves from raster image
	// Use after SFAllocMem
	//
	// Inputs: pixels (values 0-255) - pixels intensities matrix of the raster image (column by column),
	// w, h - width and height of the raster image, frameWidth = 128.
	// Outputs: pointsX, pointsY - coordinates of points of raw curves in pixels (sequentially),
	// counters - an array. Element 0 - quantity of all points of raw curves, 
	// 1 - quantity of raw curves, 2 - quantity of all processed points including rejected ones, 
	// 3 - return code of raw curves extraction function
	int SFRawVectorize(short pixels[], float pointsX[], float pointsY[],
		int counters[], int w, int h, int frameWidth)
	{
		int ret = RawVectorize(fV, pixels, w, h, frameWidth);
		int cnt = 0;
		for (int i = 0; i < fV->curvesCounter; ++i)
		{
			fV->currentCurve = fV->curves[i];
			for (int j = 0; j < fV->curves[i]->vertexID; ++j)
			{
				pointsX[cnt] = fV->curves[i]->curvePoints[j].x;
				pointsY[cnt] = fV->curves[i]->curvePoints[j].y;
				++cnt;
			}
		}
		counters[0] = cnt;
		counters[1] = fV->curvesCounter;
		counters[2] = fV->totalPointsCounter;
		counters[3] = ret;
		return 0;
	}

	// Extracts curves from raw curves (which are in memory)
	// Use after SFRawVectorize
	//
	// Outputs: curves - array of sets of numbers which describe geometrical primitives, lines or arcs
	// The curve data stride: 15
	// 0: line/curve start point x in pixels
	// 1: line/curve start point y in pixels
	// 2: line/curve finish point x in pixels
	// 3: line/curve finish point y in pixels
	// 4: curve middle point x in pixels
	// 5: curve middle point y in pixels
	// 6: line direction angle in radians
	// 7: line/curve length in pixels
	// 8: curve center point x in pixels
	// 9: curve center point y in pixels
	// 10: curve radius in pixels
	// 11: curve start angle (normal) in radians
	// 12: curve finish angle (normal) in radians
	// 13: curve turn angle (central angle) in radians
	// 14: line/curve indicator (0 line, else arc)
	int SFExtractCurves(double curves[])
	{
		//int ret = EctractCurves(cE, fV, fV->curvesCounter);
		for (int i = 0; i < cE->curvesCounter; ++i)
		{
			curves[15*i + 0] = cE->curves[i]->x1;
			curves[15*i + 1] = cE->curves[i]->y1;
			curves[15*i + 2] = cE->curves[i]->x2;
			curves[15*i + 3] = cE->curves[i]->y2;
			curves[15*i + 4] = cE->curves[i]->xM;
			curves[15*i + 5] = cE->curves[i]->yM;
			curves[15*i + 6] = cE->curves[i]->dirAngle;
			curves[15*i + 7] = cE->curves[i]->length;
			curves[15*i + 8] = cE->curves[i]->xC;
			curves[15*i + 9] = cE->curves[i]->yC;
			curves[15*i + 10] = cE->curves[i]->rC;
			curves[15*i + 11] = cE->curves[i]->angleStart;
			curves[15*i + 12] = cE->curves[i]->angleFinish;
			curves[15*i + 13] = cE->curves[i]->angleTurn;
			curves[15*i + 14] = cE->curves[i]->isArc;
		}
		return cE->curvesCounter;
	}

	// Frees all allocated memory for the current image.
	// Use after SFExtractCurves
	int SFFreeMem()
	{
		free(cE);

		for(int i = 0; i < 10001; ++i)
			free(fV->curves[i]);

		free(covGrid->coverGrid1);
		free(covGrid->coverGrid2);
		free(covGrid->coverGrid3);
		free(covGrid->coverGrid4);
		free(covGrid->coverGrid6);
		free(covGrid->coverGrid8);
		free(covGrid->coverGrid11);
		free(covGrid->coverGrid16);
		free(covGrid->coverGrid22);
		free(covGrid->coverGrid32);
		free(covGrid->coverGrid45);
		free(covGrid->coverGrid64);
		free(covGrid->coverGrid90);
		free(covGrid->coverGrid128);

		free(covGrid);
		free(fV);
		return 0;
	}
#ifdef __cplusplus
}
#endif