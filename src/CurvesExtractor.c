#include <stdio.h>
#include <math.h>
#include "CurvesExtractor.h"

static int curvesCounter;
static float x1c, y1c;
static float x2, y2;
static CurvesExtractor *cE;

void PrepareCurvesExtractor(CurvesExtractor *cE_)
{
	cE = cE_;
	for(int i = 0; i < 19999; ++i)
		cE->curves[i] = (Curve*)malloc(sizeof(Curve));
	curvesCounter = 0;
}

int EctractCurves(CurvesExtractor *cE, FastVectorizator *fV, int rawCurvesQuantity)
{
    curvesCounter = 0;
    for (int rawCurveCnt = 0; rawCurveCnt < rawCurvesQuantity; ++rawCurveCnt)
    {
        cE->currentRawCurve = fV->curves[rawCurveCnt];
        int pointsCnt = (cE->currentRawCurve->backVertexID < cE->currentRawCurve->vertexID)
            ? cE->currentRawCurve->backVertexID : cE->currentRawCurve->vertexID;
        for (int i = 0; i < cE->currentRawCurve->vertexID; ++i)
            cE->pointsAngles[i] = cE->currentRawCurve->curvePoints[i].angle;
        SearchExtractCurve(0, pointsCnt - 1);
        if (cE->currentRawCurve->backVertexID < 9000)
            SearchExtractCurve(cE->currentRawCurve->backVertexID, cE->currentRawCurve->vertexID - 1);
    }
	cE->curvesCounter = curvesCounter;
    return 0;
}

int SearchExtractCurve(int startID, int finishID)
{
    if (finishID < 0 || startID > finishID)
        return 1;
    if (curvesCounter > 19998)
        return 1;
    // making line equation
    float k = (cE->pointsAngles[finishID] - cE->pointsAngles[startID])
        / (float)(finishID - startID);
    float b = cE->pointsAngles[startID] - k * (float)startID;
    float maxOffset = 0, offset;
    int maxOffsetID = startID;
    for (int i = startID; i <= finishID; ++i)
    {
        offset = fabs(k * (float)i + b - cE->pointsAngles[i]);
        if (offset > maxOffset)
        {
            maxOffset = offset;
            maxOffsetID = i;
        }
    }
    // write curve or divide into 2 parts
    if (maxOffset < 0.30) // 0.30 default - threshold of curve division. 
		// Greater value leads to less aggressive fragmentation of curves, and longer average curve length
    {
        x1c = cE->currentRawCurve->curvePoints[startID].x;
        y1c = cE->currentRawCurve->curvePoints[startID].y;
        x2 = cE->currentRawCurve->curvePoints[finishID].x;
        y2 = cE->currentRawCurve->curvePoints[finishID].y;
        float xm, ym;
        if ((finishID - startID) % 2 == 0)
        {
            xm = cE->currentRawCurve->curvePoints[(finishID + startID) / 2].x;
            ym = cE->currentRawCurve->curvePoints[(finishID + startID) / 2].y;
        }
        else
        {
            xm = 0.5f * cE->currentRawCurve->curvePoints[(finishID + startID + 1) / 2].x
                + 0.5f * cE->currentRawCurve->curvePoints[(finishID + startID - 1) / 2].x;
            ym = 0.5f * cE->currentRawCurve->curvePoints[(finishID + startID + 1) / 2].y
                + 0.5f * cE->currentRawCurve->curvePoints[(finishID + startID - 1) / 2].y;
        }
        if (startID != finishID)
        {
            MakeCurve(cE->curves[curvesCounter], x1c, y1c, xm, ym, x2, y2);
            ++curvesCounter;
        }
    }
    else
    {
        SearchExtractCurve(startID, maxOffsetID);
        SearchExtractCurve(maxOffsetID, finishID);
    }
	return 0;
}

int GetCurvesQuantity()
{
    return curvesCounter;
}