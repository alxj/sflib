// Extracts a set of raw curves from a raster image representation (its pixels matrix)
#ifdef __cplusplus
extern "C" 
{
#endif
	#include <stdio.h>
	#include "stdlib.h"
	#include <math.h>
	#include "RawCurve.h"
	#include "CoverGrid.h"

	#ifndef _SF_FASTVECTORIZATOR_
	#define _SF_FASTVECTORIZATOR_

	typedef struct
	{
		RawCurve *curves[10001];
		RawCurve *currentCurve;

		// Coverage grid
		CoverGrid *covGrid;

		// Raster image pixels intensities 0-255
		short *pixels;

		// Quantity of raw curves
		int curvesCounter;

		// Quantity of actual points in all raw curves
		int pointsCounter;

		// Quantity of all processed points, including ignored ones
		int totalPointsCounter;

		// Maximal allowed number of curves reaching which will cause the end of raw curves extraction
		int maxCurvesNumber;

		// Contrast representation for relatively "dark" pixels detection sensitivity (approx. 0.8-0.9)
		double searchContrast;

		// <may be deprecated>
		float treshold;
	} FastVectorizator;

	// Initialization of the vectorization variables and raw curves' memory allocation
	int PrepareVectorizator(FastVectorizator *fV, CoverGrid *covGrid);

	// Precalculation of sine, cosine values of discrete possible raw curve directions angles
	void PreCalculateTrigonometry();

	int GetRawCurvesQuantity();

	int GetPointsQuantity();

	int GetTotalPointsQuantity();

	// Selects a working grid with size equals step (between last neighbour-points)
	void SelectGrid();

	// Main function of raw curves extraction from a raster image pixels matrix
	int RawVectorize(FastVectorizator *fV, short *pixels_, int w_, int h_, int frameWidth_);

	// Extracts a raw curve from its detection to termination
	void TrackCurve();

	// Performs a step - finding a new point of the current raw curve
	int GoStep();

	#endif
#ifdef __cplusplus
}
#endif