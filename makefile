CC=gcc
CFLAGS=-Wall -shared -std=c99 -fPIC

all:
	$(CC) $(CFLAGS) src/API.c src/CoverGrid.c src/Curve.c src/CurvesExtractor.c src/FastVectorizator.c src/RawCurve.c -o libsf.so